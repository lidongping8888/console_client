import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/login'
import Main from '@/pages/main'
import Index from '@/pages/index'
import List from '@/pages/application/list'
import ServerList from '@/pages/application/serverList'
import OrderList from '@/pages/order/orderList'
import Information from '@/pages/user/information'
import Character from '@/pages/user/character'
import Mechanism from '@/pages/user/mechanism'
import Board from '@/pages/user/board'
import Report from '@/pages/efficacy/report'
import Register from '@/pages/register'
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/main',
      component: Main,
      redirect: { name: 'index' },
      children: [{
        path: '/index',
        name: 'index',
        component: Index
      }]
    },
    {
      path: '/application',
      component: Main,
      redirect: { name: '/application/list' },
      children: [{
        path: '/application/list',
        name: 'appList',
        component: List
      }, {
        path: '/application/serverList',
        name: 'serverList',
        component: ServerList
      }]
    },
    {
      path: '/efficacy',
      component: Main,
      redirect: { name: '/efficacy/report' },
      children: [{
        path: '/efficacy/report',
        name: 'efficacy',
        component: Report
      }]
    },
    {
      path: '/order',
      component: Main,
      redirect: { name: '/order/list' },
      children: [{
        path: '/order/list',
        name: 'order',
        component: OrderList
      }]
    },
    {
      path: '/user',
      component: Main,
      redirect: { name: '/user/information' },
      children: [{
        path: '/user/information',
        name: 'user',
        component: Information
      }]
    },
    {
      path: '/user',
      component: Main,
      redirect: { name: '/user/character' },
      children: [{
        path: '/user/character',
        name: 'usercharacter',
        component: Character
      }]
    },
    {
      path: '/user',
      component: Main,
      redirect: { name: '/user/mechanism' },
      children: [{
        path: '/user/mechanism',
        name: 'usermechanism',
        component: Mechanism
      }]
    },
    {
      path: '/user',
      component: Main,
      redirect: { name: '/user/board' },
      children: [{
        path: '/user/board',
        name: 'userboard',
        component: Board
      }]
    }
  ]
})
