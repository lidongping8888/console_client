// TODO:import store from '@/store'
let data = [
  {
    id: '1-1',
    title: '应用管理',
    subNav: [{
      id: '1-1-1',
      title: '应用列表',
      name: '',
      url: '/application/list'
    }]
  },
  {
    id: '1-2',
    title: '套餐服务',
    subNav: [{
      id: '1-2-1',
      title: '订单发起',
      name: '订单发起',
      url: '/order/list'
    }, {
      id: '1-2-2',
      title: '发票开具',
      name: '',
      url: ''
    }, {
      id: '1-2-3',
      title: '充值提现',
      name: '',
      url: ''
    }]
  },
  {
    id: '1-3',
    title: '效能报表',
    subNav: [{
      id: '1-3-1',
      title: '基础报表',
      name: '',
      url: '/efficacy/report'
    }]
  },
  {
    id: '1-4',
    title: '账户中心',
    subNav: [{
      id: '1-4-1',
      title: '基础资料',
      name: '',
      url: '/user/information'
    }, {
      id: '1-4-2',
      title: '机构与账户管理',
      name: '',
      url: '/user/mechanism'
    }, {
      id: '1-4-3',
      title: '角色与权限管理',
      name: '',
      url: '/user/character'
    },{
      id: '1-4-4',
      title: '菜单管理',
      name: '',
      url: '/user/board'
    }]
  },
]
export default data
