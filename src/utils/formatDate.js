function formatDate (timestr, fmt) {
  let date = new Date(timestr)
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  let o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + ''
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str))
    }
  }
  return fmt
};

function padLeftZero (str) {
  return ('00' + str).substr(str.length)
}
function getDateByYMD () {
  let date = new Date()
  let y = date.getFullYear() + '-'
  let m = date.getMonth() + 1 + '-'
  let d = date.getDate()
  return y + m + d
}
function getBeforeFourYear (isCurrentYear) {
  let years = []
  // console.log(formatDate(new Date())
  for (let i = 0; i < 4; i++) {
    if (isCurrentYear) {
      years.push({ id: formatDate(new Date(), 'yyyy') - i, name: formatDate(new Date(), 'yyyy') - i, type: 'year' })
    } else {
      years.push({ id: formatDate(new Date(), 'yyyy') - (i + 1), name: formatDate(new Date(), 'yyyy') - (i + 1), type: 'year' })
    }
  }
  return years
}
export { formatDate, getDateByYMD, getBeforeFourYear }
