export default {
  /* 登录接口 */
  login: 'api/login',
  getAppList: 'api/application/listApplication',
  addApp: '/api/application/addApplication',
  editApp: '/api/application/updateMdfTbApplication',
  getServerList: '/api/server/listSourceServer',
  getProtoData: 'api/server/listMdfTbProtocol',
  getRegionsData: 'api/server/listMdfTbCountry',
  addServer: 'api/server/addSourceServer',
  editServer: 'api/server/updateMdfTbSourceServer',
  deleteServer: 'api/server/deleteMdfTbSourceServer',
  orderList: '/api/order/listOrder',
  getSpeeds: 'api/order/listSpeed',
  getActives: 'api/order/listRihuo',
  renewalMdfTbOrder: 'api/order/renewalMdfTbOrder',
  getAppForSelec: 'api/order/getMdfTbApplicationByUserId',
  addOrder: 'api/order/insertOrder',
  editOrder: 'api/order/updateMdfTbOrder',
  updateOrderStatus: 'api/order/updateOrderStatus',
  pendingOrderStatus: '/api/order/orderEffect',
  getuserId: 'api/user/getUser',
  uploadfile: 'api/user/insertFile',
  getTableList: 'api/role/listMdfTbRole',
  addUser:'api/role/insertMdfTbRole',
  saveUserBasic:'/api/user/insertMdfUserBasi',
  deleteMdfTbmenu:'api/menu/deleteMdfMenu',
  deleteMdfTbrole:'api/role/deleteMdfTbRole',
  listMdfTbmenu:'api/menu/listMdfTbMenu',
  doassign:'api/menu/doAssign',
  loadassoignData:'api/menu/loadAssoignData',
  listmenutree:'api/dept/listMenuTree',
  getDeptIdUser: '/api/dept/getDeptIdUser',
  insertAndUserStatus: 'api/dept/insertAndUserStatus',
  getApplicationName: 'api/report/getApplicationName',
  insertAndUpdateDept: '/api/dept/insertAndUpdateDept'
}
