let axios = require('axios')
axios.defaults.timeout = 15000
axios.defaults.maxContentLength = 1048576
axios.defaults['Content-Type'] = 'application/json;charset=UTF-8'
axios.defaults.baseURL = (function () {
  let baseURL
  if (process.env.NODE_ENV === 'development') {
    // baseURL = 'http://testapp.zhiyazhiyuan.com:8763/'
    // baseURL = 'http://192.168.0.189:5566/'
    baseURL = 'http://192.168.0.189:5566/'
  } else if (process.env.NODE_ENV === 'production') {
    baseURL = 'https://wechat.junyanginfo.com'
  }
  return baseURL
})()

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})
// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response.data
}, function (error) {
  return Promise.reject(error)
})

export default axios
